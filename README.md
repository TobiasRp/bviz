# bviz README #

### What is this repository for? ###

BViz is a small tool for visualizing barycentric coordinates

Please note that this is a helper tool created for my master thesis and consists of lazily ported C++/CUDA code
and is in general not production quality code.
