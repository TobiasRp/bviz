﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace BViz
{
    public class Triangle
    {
        private static readonly Color BackgroundColor = Color.FromRgb(190, 220, 255);

        public Polygon Poly { get; set; }

        public readonly List<BPoint> Points;
        public readonly List<BPoint> NewPoints;

        public Vector3 PartialX, PartialY;

        public Triangle(Point p0, Point p1, Point p2)
        {
            Poly = new Polygon
            {
                Stroke = SystemColors.WindowFrameBrush,
                Fill = new SolidColorBrush(BackgroundColor)
            };

            Poly.Points.Add(p0);
            Poly.Points.Add(p1);
            Poly.Points.Add(p2);

            Points = new List<BPoint>();
            NewPoints = new List<BPoint>();
        }

        public void Resize(Point p0, Point p1, Point p2)
        {
            Poly.Points[0] = p0;
            Poly.Points[1] = p1;
            Poly.Points[2] = p2;
        }

        public void ClearAllPoints()
        {
            Points.Clear();
            NewPoints.Clear();
        }

        public void MergeNewPoints()
        {
            Points.AddRange(NewPoints);
            NewPoints.Clear();
        }

        public int GetNumTotalPoints()
        {
            return Points.Count + NewPoints.Count;
        }

        public void CalcPartialDerivatives(BPoint p, BPoint pX1, BPoint pY1)
        {
            PartialX = new Vector3(pX1.B0 - p.B0, pX1.B1 - p.B1, pX1.B2 - p.B2);
            PartialY = new Vector3(pY1.B0 - p.B0, pY1.B1 - p.B1, pY1.B2 - p.B2);
        }

    }
}
