﻿using System;
using System.Windows;
using System.Collections.Generic;

namespace BViz
{
    public class SamplingSqrt5 : Sampling
    {

        private List<BPoint> SampleSqrt5Coeffs(BPoint bp, Triangle triangle, double coeffU,
            double coeffV, int step)
        {
            List<BPoint> res = new List<BPoint>(4);
            var pt = BPoint.Convert(triangle, bp);

            var u1 = new Point(coeffU * step, coeffV * step);
            var v1 = new Point(-coeffV * step, coeffU * step);

            var newPoints = new List<Point>(4);
            newPoints.Add(new Point(pt.X + u1.X, pt.Y + u1.Y));
            newPoints.Add(new Point(pt.X + v1.X, pt.Y + v1.Y));
            newPoints.Add(new Point(pt.X - u1.X, pt.Y - u1.Y));
            newPoints.Add(new Point(pt.X - v1.X, pt.Y - v1.Y));

            foreach (var newPt in newPoints)
            {
                res.Add(BPoint.ConvertFrom(triangle, newPt));
            }

            return res;
        }

        public override List<BPoint> Sample(BPoint bp, Triangle triangle, int pixelSize)
        {
            switch (RecursionLevel)
            {
                case 0:
                    return SampleSqrt5Coeffs(bp, triangle, 0.4, 0.2, pixelSize);
                case 1:
                    return SampleSqrt5Coeffs(bp, triangle, 2.0 / 25, 4.0 / 25, pixelSize);
                case 2:
                    return SampleSqrt5Coeffs(bp, triangle, 2.0 / 125, 11.0 / 125, pixelSize);
                default:
                    throw new NotImplementedException("Iteration level > 2 not implemented");
            }
        }

        private void AddPointInside(List<BPoint> res, BPoint bp)
        {
            if (bp.IsInside())
                res.Add(bp);
        }

        private List<BPoint> SampleSqrt5Bary(BPoint bp, Vector3 partialX, Vector3 partialY, double cU, double cV)
        {
            List<BPoint> res = new List<BPoint>(4);

            var coeffUx = cU * partialX.X + cV * partialY.X;
            var coeffUy = cU * partialX.Y + cV * partialY.Y;

            var coeffVx = -cV * partialX.X + cU * partialY.X;
            var coeffVy = -cV * partialX.Y + cU * partialY.Y;

            AddPointInside(res, new BPoint(bp.B0 + coeffUx, bp.B1 + coeffUy));
            AddPointInside(res, new BPoint(bp.B0 + coeffVx, bp.B1 + coeffVy));

            AddPointInside(res, new BPoint(bp.B0 - coeffUx, bp.B1 - coeffUy));
            AddPointInside(res, new BPoint(bp.B0 - coeffVx, bp.B1 - coeffVy));

            return res;
        }

        public override List<BPoint> Sample(BPoint bp, Vector3 partialX, Vector3 partialY)
        {
            switch (RecursionLevel)
            {
                case 0:
                    return SampleSqrt5Bary(bp, partialX, partialY, 0.4, 0.2);
                case 1:
                    return SampleSqrt5Bary(bp, partialX, partialY, 2.0 / 25, 4.0 / 25);
                case 2:
                    return SampleSqrt5Bary(bp, partialX, partialY, 2.0 / 125, 11.0 / 125);
                default:
                    throw new NotImplementedException("Iteration level > 2 not implemented");
            }
        }
    }
}
