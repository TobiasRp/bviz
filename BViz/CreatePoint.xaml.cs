﻿using System;
using System.Windows;

namespace BViz
{
    /// <summary>
    /// Interaction logic for CreatePoint.xaml
    /// </summary>
    public partial class CreatePoint : Window
    {
        public BPoint NewPoint { get; set; }

        public CreatePoint()
        {
            InitializeComponent();
        }

        private void Add_OnClick(object sender, RoutedEventArgs e)
        {
            Double b0 = Convert.ToDouble(Box0.Text);
            Double b1 = Convert.ToDouble(Box1.Text);
            Double b2 = Convert.ToDouble(Box2.Text);

            try
            {
                NewPoint = new BPoint(b0, b1, b2);
            }
            catch (ArgumentException)
            {
                return;
            }

            DialogResult = true;
        }

        private void Cancel_OnClick_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
