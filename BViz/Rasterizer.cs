﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BViz
{
    public class Rasterizer
    {
        public static readonly int RastStepSize = 60;
        public static readonly int RastLineHeight = (RastStepSize / 2) * 5;


        private static PointSS TrianglePoint(int screenHeight, Triangle triangle, int index)
        {
            PointSS p = new PointSS(triangle.Poly.Points[index]);
            p.Y = screenHeight - p.Y;
            return p;
        }

        public static void RasterizeTriangle(Triangle triangle, PointSS start, PointSS end,
            int screenHeight)
        {
            var p0 = TrianglePoint(screenHeight, triangle, 0);
            var p1 = TrianglePoint(screenHeight, triangle, 1);
            var p2 = TrianglePoint(screenHeight, triangle, 2);

            EdgeFunction e01 = new EdgeFunction();
            EdgeFunction e12 = new EdgeFunction();
            EdgeFunction e20 = new EdgeFunction();

            int w0Row = e01.Eval(p0, p1, start, RastStepSize, RastStepSize);
            int w1Row = e12.Eval(p1, p2, start, RastStepSize, RastStepSize);
            int w2Row = e20.Eval(p2, p0, start, RastStepSize, RastStepSize);

            BPoint wOrigin = new BPoint(w1Row, w2Row, w0Row);
            BPoint wUp = new BPoint(w1Row + e12.OneStepY, w2Row + e20.OneStepY, w0Row + e01.OneStepY);
            BPoint wRight = new BPoint(w1Row + e12.OneStepX, w2Row + e20.OneStepX, w0Row + e01.OneStepX);

            triangle.CalcPartialDerivatives(wOrigin, wRight, wUp);

            PointSS p;
            for (p.Y = start.Y; p.Y < end.Y; p.Y += RastStepSize)
            {
                int w0 = w0Row;
                int w1 = w1Row;
                int w2 = w2Row;

                for (p.X = start.X; p.X < end.X; p.X += RastStepSize)
                {
                    if (w0 >= 0 && w1 >= 0 && w2 >= 0)
                    {
                        var pt = new BPoint(w1, w2, w0);
                        triangle.NewPoints.Add(pt);
                    }

                    w0 += e01.OneStepX;
                    w1 += e12.OneStepX;
                    w2 += e20.OneStepX;
                }
                w0Row += e01.OneStepY;
                w1Row += e12.OneStepY;
                w2Row += e20.OneStepY;
            }
        }
    }
}
