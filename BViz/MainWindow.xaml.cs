﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace BViz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<Triangle> _triangles;

        private readonly Sampling _sampler;

        private readonly SolidColorBrush _pointColorBrush;
        private readonly SolidColorBrush _newBrush;

        private readonly SolidColorBrush _backgroundGridBrush;

        private static readonly double PointDrawRadius = 5.0;

        private static readonly string IterationNotImplemented = "Sampling iteration not implemented";

        public MainWindow()
        {
            InitializeComponent();

            _triangles = new List<Triangle>();
            DrawInitialTriangles();

            _sampler = new SamplingSqrt5();

            _pointColorBrush = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            _newBrush = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            _backgroundGridBrush = new SolidColorBrush(Color.FromRgb(200, 200, 200));
        }

        /// <summary>
        /// Create/Add triangles here.
        /// </summary>
        /// <returns>Returns a list of vertices with each 3 following vertices forming a triangle</returns>
        private List<Point> CreateTriangleVertices()
        {
            var res = new List<Point>();

            var p0 = new Point(0, Surface.ActualHeight);
            var p1 = new Point(Surface.ActualWidth, Surface.ActualHeight);
            var p2 = new Point(Surface.ActualWidth / 2.0, 0);

            res.Add(p0);
            res.Add(p1);
            res.Add(p2);

            //res.Add(p1);
            //res.Add(new Point(Surface.ActualWidth, 0));
            //res.Add(p2);

            // Triangle in the upper left corner
            //res.Add(p2);
            //res.Add(new Point(0, 0));
            //res.Add(p0);

            return res;
        }

        private void DrawBackgroundPixels()
        {

            for (long x = 0; x < Surface.ActualWidth; x += Rasterizer.RastStepSize)
            {
                Line line = new Line();
                line.Stroke = _backgroundGridBrush;
                line.X1 = x;
                line.Y1 = 0;
                line.X2 = x;
                line.Y2 = Surface.ActualHeight;

                Surface.Children.Add(line);
            }
            for (long y = 0; y < Surface.ActualHeight; y += Rasterizer.RastStepSize)
            {
                Line line = new Line();
                line.Stroke = _backgroundGridBrush;
                line.X1 = 0;
                line.Y1 = Surface.ActualHeight - y;
                line.X2 = Surface.ActualWidth;
                line.Y2 = Surface.ActualHeight - y;

                Surface.Children.Add(line);
            }
        }

        private void DrawInitialTriangles()
        {
            var vertices = CreateTriangleVertices();
            for (int v = 0; v < vertices.Count; v += 3)
                _triangles.Add(new Triangle(vertices[v], vertices[v + 1], vertices[v + 2]));

            foreach (var tri in _triangles)
                Surface.Children.Add(tri.Poly);
        }

        private void UpdateNumPointsLabel()
        {
            int count = 0;
            foreach (var tri in _triangles)
                count += tri.GetNumTotalPoints();
            NumPointsLabel.Content = count + " points";
        }

        private void Surface_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void MainWindow_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Surface.Children.Clear();

            var vertices = CreateTriangleVertices();
            for (int tri = 0, v = 0; v < vertices.Count; v += 3, ++tri)
                _triangles[tri].Resize(vertices[v], vertices[v + 1], vertices[v + 2]);

            foreach (var tri in _triangles)
            {
                tri.Points.Clear();
                tri.NewPoints.Clear();
                Surface.Children.Add(tri.Poly);
            }

            UpdateNumPointsLabel();
            DrawBackgroundPixels();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            CreatePoint create = new CreatePoint();

            bool? res = create.ShowDialog();
            if (res.HasValue && (bool)res)
            {
                _triangles[0].NewPoints.Add(create.NewPoint);
                Draw(_triangles[0], create.NewPoint, _pointColorBrush);
                UpdateNumPointsLabel();
            }
        }

        private void Draw(Triangle triangle, BPoint pt, SolidColorBrush brush)
        {
            Point screenPt = BPoint.Convert(triangle, pt);

            Line line = new Line();
            line.Stroke = brush;
            line.X1 = screenPt.X - PointDrawRadius;
            line.Y1 = screenPt.Y;
            line.X2 = screenPt.X + PointDrawRadius;
            line.Y2 = screenPt.Y;

            Surface.Children.Add(line);

            line = new Line();
            line.Stroke = brush;
            line.X1 = screenPt.X;
            line.Y1 = screenPt.Y - PointDrawRadius;
            line.X2 = screenPt.X;
            line.Y2 = screenPt.Y + PointDrawRadius;

            Surface.Children.Add(line);
        }

        private void SampleTriangle(Triangle triangle)
        {
            if (triangle.PartialX == null)
                return;

            try
            {
                triangle.MergeNewPoints();

                foreach (var pt in triangle.Points)
                {
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));
                }

                DrawNewPoints(triangle, _newBrush);
                UpdateNumPointsLabel();
            }
            catch (NotImplementedException)
            {
                NumPointsLabel.Content = IterationNotImplemented;
            }
        }

        private void Sample5_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var tri in _triangles)
                SampleTriangle(tri);
            _sampler.IncreaseSampleLevel();
        }

        private void SampleOne_OnClick(object sender, RoutedEventArgs e)
        {
            if (_triangles[0].PartialX == null)
                return;

            var triangle = _triangles[0];
            try
            {
                triangle.MergeNewPoints();

                //var pt = triangle.Points[triangle.Points.Count/2];
                var pt = triangle.Points[15];
                triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));

                if (triangle.Points.Count <= 22)
                {
                    pt = triangle.Points[12];
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));
                }

                if (triangle.Points.Count > 22)
                {
                    pt = triangle.Points[22];
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));

                    pt = triangle.Points[23];
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));

                    pt = triangle.Points[24];
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));

                    pt = triangle.Points[25];
                    triangle.NewPoints.AddRange(_sampler.Sample(pt, triangle.PartialX, triangle.PartialY));
                }

                DrawNewPoints(triangle, _newBrush);
                UpdateNumPointsLabel();
            }
            catch (NotImplementedException)
            {
                NumPointsLabel.Content = IterationNotImplemented;
            }
            _sampler.IncreaseSampleLevel();
        }

        private void DrawNewPoints(Triangle triangle, SolidColorBrush brush)
        {
            foreach (var newPt in triangle.NewPoints)
            {
                Draw(triangle, newPt, brush);
            }
        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            Surface.Children.Clear();

            foreach (var tri in _triangles)
            {
                tri.ClearAllPoints();
                Surface.Children.Add(tri.Poly);
            }
            UpdateNumPointsLabel();
            DrawBackgroundPixels();
        }

        private void RasterizeAllTriangles(PointSS start, PointSS end)
        {
            _sampler.ResetSampleLevel();

            foreach (var tri in _triangles)
            {
                Rasterizer.RasterizeTriangle(tri, start, end, (int)Surface.ActualHeight);

                DrawNewPoints(tri, _pointColorBrush);
            }

            UpdateNumPointsLabel();
        }

        private void Rasterize_OnClick(object sender, RoutedEventArgs e)
        {
            int screenWidth = (int)Surface.ActualWidth;
            int screenHeight = (int)Surface.ActualHeight;

            RasterizeAllTriangles(new PointSS(Rasterizer.RastStepSize / 2,
                    Rasterizer.RastStepSize / 2),
                    new PointSS(screenWidth, screenHeight));
        }

        private void LineRast_OnClick(object sender, RoutedEventArgs e)
        {
            int screenWidth = (int)Surface.ActualWidth;

            RasterizeAllTriangles(new PointSS(Rasterizer.RastStepSize / 2,
                Rasterizer.RastLineHeight),
                new PointSS(screenWidth, Rasterizer.RastLineHeight + 1));
        }

    }
}
