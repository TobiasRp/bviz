﻿using System.Windows;

namespace BViz
{
    public struct PointSS
    {
        public int X;
        public int Y;

        public PointSS(int x, int y)
        {
            X = x;
            Y = y;
        }

        public PointSS(Point pt)
        {
            X = (int) pt.X;
            Y = (int) pt.Y;
        }
    }

    public class EdgeFunction
    {
        public int OneStepX;
	    public int OneStepY;

        public int Eval(PointSS v0, PointSS v1, PointSS origin,
            int stepSizeX, int stepSizeY)
        {
            // Edge setup
            int A = v0.Y - v1.Y;
            int B = v1.X - v0.X;
            int C = v0.X * v1.Y - v0.Y * v1.X;

            // Step deltas
            OneStepX = A * stepSizeX;
            OneStepY = B * stepSizeY;

            // Edge function values at origin
            return A * origin.X + B * origin.Y  + C;
        }
    }
}
