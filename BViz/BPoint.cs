﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace BViz
{
    public class BPoint
    {
        public double B0, B1, B2;

        public BPoint(double c0, double c1)
        {
            B0 = c0;
            B1 = c1;
            B2 = 1.0 - c0 - c1;
        }

        public BPoint(double c0, double c1 , double c2 )
        {
            if (c0 == 0.0 && c1 == 0.0 && c2 == 0.0)
                throw new ArgumentException("Invalid coordinates");

            double sum = c0 + c1 + c2;
            if (Math.Abs(1.0 - sum) >= 1e-3)
            {
                B0 = c0/sum;
                B1 = c1/sum;
                B2 = c2/sum;
            }
            else
            {
                B0 = c0;
                B1 = c1;
                B2 = c2;                
            }
        }

        public bool IsInside()
        {
            return B0 >= 0 && B1 >= 0 && B2 >= 0;
        }

        public static Point Convert(Triangle referenceTriangle, BPoint b)
        {
            double x = referenceTriangle.Poly.Points[0].X*b.B0
                       + referenceTriangle.Poly.Points[1].X*b.B1
                       + referenceTriangle.Poly.Points[2].X*b.B2;

            double y = referenceTriangle.Poly.Points[0].Y * b.B0
                       + referenceTriangle.Poly.Points[1].Y * b.B1
                       + referenceTriangle.Poly.Points[2].Y * b.B2;

            return new Point(x, y);
        }

        public static BPoint ConvertFrom(Triangle refTriangle, Point pt)
        {
            var v1 = refTriangle.Poly.Points[0];
            var v2 = refTriangle.Poly.Points[1];
            var v3 = refTriangle.Poly.Points[2];

            var b0 = ((v2.Y - v3.Y) * (pt.X - v3.X) + (v3.X - v2.X) * (pt.Y - v3.Y))
                / ((v2.Y - v3.Y) * (v1.X - v3.X) + (v3.X - v2.X) * (v1.Y - v3.Y));

            var b1 = ((v3.Y - v1.Y) * (pt.X - v3.X) + (v1.X - v3.X) * (pt.Y - v3.Y))
                / ((v2.Y - v3.Y) * (v1.X - v3.X) + (v3.X - v2.X) * (v1.Y - v3.Y));

            var b2 = 1.0 - b0 - b1;
            return new BPoint(b0, b1, b2);
        }
    }
}
