﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BViz
{
    public class Vector3
    {
        public double X, Y, Z;

        public Vector3(double x, double y, double z)
        {
            X = x; Y = y; Z = z;
        }
    }
}
