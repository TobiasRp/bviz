﻿using System;
using System.Collections.Generic;

namespace BViz
{
    public abstract class Sampling
    {
        protected int RecursionLevel;

        public Sampling()
        {
            RecursionLevel = 0;
        }

        public void ResetSampleLevel()
        {
            RecursionLevel = 0;
        }

        public void IncreaseSampleLevel()
        {
            RecursionLevel++;
        }

        public abstract List<BPoint> Sample(BPoint bp, Triangle triangle, int pixelSize);

        public abstract List<BPoint> Sample(BPoint bp, Vector3 partialX, Vector3 partialY);

    }
}
